// class Notification
function send(uuid, type, message, data){
  if(type == 1){
    Push.ios(uuid, message, 1, 'honk.wav', 'Open', data)
  }else{
    Push.android(uuid, message, 1, 'default', 'Open', data)
  }
}

// class Driver
function async setStatus(driverId, available){
  await Driver.update(driverId, {
    'available': available
  })
}

// class Service
function async assignServiceToDriver(service, driver, statusId){
  const data = {
    'statusId': statusId,
    'driverId': driver.id,
    'car_id': driver.car_id
  }
  await Driver.setStatus(driver.id, 0)
  return await Service.update(service.id, data)
}

// Service controller
async function confirmService(){
  const serviceId = req.body.serviceId
  const driverId = req.body.driverId
  let user, service, driver

  // find service
  service = await Service.find(serviceId)
  if(!service) return res.status(500).json({error: 'El servicio no se ha encontrado :c'})
  if(service.driver_id || service.status_id != 1) return res.json({msg: 'El servicio ya esta asignado'})

  // find driver
  driver = await Driver.find(driverId)
  if(!driver) return res.status(500).json({error: 'El conductor no existe'})

  // find user uuid
  user = await User.find(service.userId)
  if(!user.uuid) return res.status(500).json({error: 'No existe uuid para este usuario'})

  // assign service to driver
  service = await assignServiceToDriver(service, driver, 2)

  // send notification to the user
  Push.send(user.uuid, user.type, 'Tu servicio ha sido confirmado', {serviceId})

  return res.json({msg: 'El servicio fue confirmado correctamente'})
}