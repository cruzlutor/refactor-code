# Malas practicas
* **Integridad de la información**: no se comprueba la existencia de todas las entidades que vienen por input del usuario
* **Alto acoplamiento**: al reducir el acoplamiento se obtienen funciones modulares y escalables, tambien facilita el debuging y el testing
* **Estados de respuesta**: no se asigna un estado apropiado a la respuestas (Success, Fail), muchos clientes esperan un estado en su peticion para manejar errores
* **Organizacion y comentarios**: es importante que el codigo sea legible por humanos

El pseudocodigo en confirm.js evita todas las malas practicas previamente listadas

# En que consiste el principio de responsabilidad unica? 
Cada clase o modulo debe tener una responsabilidad especifica, encargarse de resolver una caracteristica puntual del sistema, esto disminuye el nivel de acoplamiento comentado previamente

# Que caracteristicas tiene un buen codigo?
* Organizacion
* Comentarios
* Modularizacion
* Patrones de diseño
* Pruebas unitarias
* Lint, Styleguide o alguna convencion definida por la comunidad
